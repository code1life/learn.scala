package ch2funcprog

object FuncAsParameter {
  def main(args: Array[String]): Unit = {
    def plus(num: Int): Int = {
      num + 1
    }

    println(Array(1, 2, 3, 4).map(plus(_)).mkString(","))
    println(plus _)



    val doubleNum = (x: Int) => x * 2 //匿名函数
    println(doubleNum(2))
    println(doubleNum)
  }
}
