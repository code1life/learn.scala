package ch2funcprog

object CurryFunc {
  def main(args: Array[String]): Unit = {
    def plus(num1: Int) = { //高阶函数
      (num2: Int) => num1 * num1 + num2 //闭包
    }

    println(plus(1)(2)) //plus(1)返回的是个匿名函数（num2:Int）=> 1 + num2，继续调用(2) //柯里化

    //闭包
    val file = mkSuffix(".jpg")
    println(file("dog.jpg"))
    println(file("cat"))


    //普通函数
    def foo1(num1: Int, num2: Int) = num1 * num2
    println(foo1(2, 2))

    //闭包
    def foo2(num1: Int) = { num2: Int => num1 + num2 }
    println(foo2(2)(2))

    //柯里化（更简单的写法）
    def curryFunc(x: Int)(y: Int) = x * y
    println(curryFunc(2)(2))

    /*
    两个字符串在忽略大小写时是否相等
     */
    def compare2Str(str1:String)(str2:String)= str1.toLowerCase().equals(str2.toLowerCase())
    println(compare2Str("aBc")("AbC"))
  }

  def mkSuffix(suffix: String) = {
    (filename: String) => {
      if (filename.endsWith(suffix)) filename else filename + suffix
    }
  }
}
