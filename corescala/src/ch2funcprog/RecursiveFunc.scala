package ch2funcprog

object RecursiveFunc {
  def main(args: Array[String]): Unit = {
    val str = "abcdefg"
    println(reverseStr(str))

    var count = 0

    def fib(num: Int): Int = {
      count += 1
      if (num == 1 || num == 2) {
        1
      } else {
        fib(num - 1) + fib(num - 2)//存在重复计算->迭代
      }
    }

    println("fib(20):" + fib(20) + "\tcount:" + count)
    println("fib(21):" + fib(21) + "\tcount:" + count)
    println("fib(22):" + fib(22) + "\tcount:" + count)
  }

  def reverseStr(str: String): String = {
    if (str.length == 1) str else reverseStr(str.tail) + str.head
  }
}
