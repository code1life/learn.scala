package ch2funcprog

import scala.collection.mutable

object CountBaseNumber {
  def main(args: Array[String]): Unit = {
    val sry = "gatctataga gtgagttcca agaaagctag ggctacacag agaaaccctg ttttgaaaaa"

    def countBaseNum(m: mutable.Map[Char, Int], b: Char): mutable.Map[Char, Int] = {
      m + (b -> (m.getOrElse(b, 0) + 1))//map+(k->v)
    }

    val numMap = sry.foldLeft(mutable.Map[Char, Int]())(countBaseNum)
    println(numMap)

  }
}
