package ch2funcprog

object ParameterInfer {
  def main(args: Array[String]): Unit = {
    val list = List(1, 2, 3, 4)
    println(list.map((x: Int) => x + 1)) //匿名函数
    println(list.map((x) => x + 1)) //参数类型可以推断
    println(list.map(x => x + 1)) //一个参数可以不写括号
    println(list.map(_ + 1)) //x只出现一次，可以省略

    println(list.reduce(_ + _)) //reduce是一个高阶函数，因此可以传入函数
    println(list.reduce(foo))
    println(list.reduce((num1: Int, num2: Int) => num1 + num2))
    println(list.reduce((num1, num2) => num1 + num2)) //num1,num2只出现一次, println(list.reduce((num1, num2) => num1*num1 + num2)),则不能用_代替

    println(list.reduce(_ max _))
    println(list.max)
  }

  def foo(num1: Int, num2: Int): Int = {
    num1 + num2
  }
}
