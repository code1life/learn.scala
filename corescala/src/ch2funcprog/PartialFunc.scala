package ch2funcprog

object PartialFunc {
  def main(args: Array[String]): Unit = {
    val list = List(1, 2, 3, 4, "abc") //实现对list中的数字加1操作

    //1:filter-map
    println(list.filter(filterNum).map(forMap).map(addOne))
    //2:模式匹配
    println(list.map(forMatch))

    //偏函数
    val partialFunc1 = new PartialFunction[Any, Int] {
      override def isDefinedAt(x: Any): Boolean = {
        println("isDefinedAt(x: Any)");
        x.isInstanceOf[Int]
      } //调用5次

      override def apply(v1: Any) = {
        println("apply(v1: Any)");
        v1.asInstanceOf[Int] + 1
      } //调用4次
    }
    println(list.collect(partialFunc1))

    //偏函数简写1
    println(list.collect(partialFunc2))
    //偏函数简写2
    println(list.collect{ case i: Int => i * 2 })//println(list.collect{ case i: Int => i * 2; }),可以写多个处理

    val list2 = List(1, 2, 3, 1.2, "Hello")
    println(list2.collect(partialFunc3))

  }

  def filterNum(item: Any): Boolean = {
    item.isInstanceOf[Int]
  }

  def forMap(item: Any): Int = {
    item.asInstanceOf[Int]
  }

  def addOne(num: Int): Int = {
    num + 1
  }

  def forMatch(item: Any): Any = {
    item match {
      case num: Int => num + 1
      case _ =>
    }
  }


  def partialFunc2: PartialFunction[Any, Int] = {
    case i: Int => i + 1
  }

  def partialFunc3: PartialFunction[Any, Any] = {
    case i: Int => i + 1
    case i: Double => i * 2
  }

}
