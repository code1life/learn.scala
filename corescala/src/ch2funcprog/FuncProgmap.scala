package ch2funcprog

object FuncProgmap {
  def main(args: Array[String]): Unit = {
    val list = List(1, 2, 3, 4, 5)
    println(list.map(tripleNum))
  }

  def tripleNum(num: Int): Int = {
    println("exe: tripleNum(num: Int): Int")
    num * num * num
  }
}
