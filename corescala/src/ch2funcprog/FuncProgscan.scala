package ch2funcprog

object FuncProgscan {
  def main(args: Array[String]): Unit = {
    val list = List(1, 2, 3, 4)
    println(list.scan(5)(sum))
    println(list.scanRight(5)(sum))
  }

  def sum(n1: Int, n2: Int): Int = {
    println(s"n1+n2:\t${n1 + n2}")
    n1 + n2
  }

}
