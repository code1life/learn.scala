package ch2funcprog

object MultiUsingUnicode {
  def main(args: Array[String]): Unit = {
    var res1 = 1L
    "Hello".foreach(res1 *= _.toLong)//"Hello".foreach((_) => {res1 *= _.toLong})


    var res2 = 1L
    def trans4Uni(i: Char): Unit = {
      res2 *= i.toLong
    }

    "Hello".foreach(trans4Uni)

    println(s"${res1}\t${res2}")
  }

}
