package ch2funcprog

object RecursiveMethod {
  def main(args: Array[String]): Unit = {
    fun(5)
    println(fibonacci(10))
  }

  def fun(n: Int) {
    if (n > 2) {
      fun(n - 1)
    }
    println("n=" + n)
  }

  def fibonacci(n: Int): Int = {
    if (n == 1 || n == 2) {
      1
    } else {
      fibonacci(n - 1) + fibonacci(n - 2)
    }
  }
}
