package ch2funcprog

object FuncProgfold {
  def main(args: Array[String]): Unit = {
    val list = List(1, 2, 3, 4, 5)
    println(list.fold(6)(minus))//函数柯里化
    println((6/:list)(minus))//上面的等价简写版本

    println(list.foldRight(6)(minus))
    println((list:\6)(minus))
  }

  def minus(num1: Int, num2: Int): Int = {
    println(s"num1 - num2:\t${num1 - num2}")
    num1 - num2
  }
}
