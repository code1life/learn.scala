package ch2funcprog

object Method2Function {
  def main(args: Array[String]): Unit = {
    val t = new Test
    println(t.sum(10, 20))

    val func1 = t.sum _
    println(func1(10, 20))

    val func2 = (n1: Int, n2: Int) => {
      n1 + n2
    }
    println(func2(10, 20))
  }

}

class Test {
  def sum(n1: Int, n2: Int): Int = {
    n1 + n2
  }
}
