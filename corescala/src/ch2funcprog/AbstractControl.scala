package ch2funcprog

object AbstractControl {
  def main(args: Array[String]): Unit = {

    //将一段代码块作为参数传递给高阶函数
    myRun1 {
      //抽象控制函数（参数是函数，且这个函数没有参数和返回值）
      () =>
        println("myRun1:开启线程")
        Thread.sleep(3000)
        println("myRun1:线程执行完毕")
    }

    //简写
    myRun2 {
      //抽象控制
      println("myRun2:开启线程")
      Thread.sleep(3000)
      println("myRun2:线程执行完毕") //大括号包括的代码块，
    }


    var num = 5
    while (num > 0) {
      println(s"num:${num -= 1; num}")
    }

    def untilFunc(condition: => Boolean)(block: => Unit): Unit = {//while底层也是类似的实现方式
      if (condition) {
        block
        untilFunc(condition)(block)
      }
    }
    num = 5
    untilFunc(num > 0) {//(num>0)其实是传入匿名函数（看函数声明）
      println(s"untilFunc:${num -= 1;num}")
    }

  }

  def myRun1(foo: () => Unit) = {
    new Thread {
      override def run(): Unit = {
        foo() //调用
      }
    }.start()
  }

  def myRun2(foo: => Unit) = {
    //本质仍然是传入匿名函数（简化：不写()）
    new Thread() {
      override def run(): Unit = {
        foo //
      }
    }.start()
  }
}
