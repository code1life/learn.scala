package ch2funcprog

object CountDown {

  def main(args: Array[String]): Unit = {
    for (i <- 1 to 10 reverse){println(s"i:\t${i}")}

    (0 to 10).reverse.foreach(println)

    (0 to 10).reverse.foreach(myPrint)
  }
  def myPrint(i:Int): Unit ={
    println("我被调用了")
    println(i)
  }
}
