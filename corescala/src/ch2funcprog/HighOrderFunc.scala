package ch2funcprog

object HighOrderFunc {
  def main(args: Array[String]): Unit = {
    println(s"foo1(3.3):${foo1(3.3)}")
    println(s"foo2(foo1,3.3):${foo2(foo1, 3.3)}")
    println(s"${foo4(foo3, 3)}")
  }

  def foo1(num: Double) = {
    num * 2
  }

  def foo2(f: Double => Double, num: Double) = {
    f(num) + 3
  }

  def foo3() = {
    println("log--------")
  }

  def foo4(f: () => Unit, num: Double) = {
    f()
    num * 2
  }
}
