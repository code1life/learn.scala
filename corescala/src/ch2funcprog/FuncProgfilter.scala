package ch2funcprog

object FuncProgfilter {
  def main(args: Array[String]): Unit = {
    val list = List("Adam", "Bob", "Cherry", "Ding")
    println(list.filter(lenFilter))

  }

  def lenFilter(str: String): Boolean = {
    str.length > 3
  }
}
