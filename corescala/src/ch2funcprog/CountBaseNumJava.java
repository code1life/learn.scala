package ch2funcprog;

import java.util.HashMap;
import java.util.Map;

public class CountBaseNumJava {
    public static void main(String[] args) {
        String sry = "gatctataga gtgagttcca agaaagctag ggctacacag agaaaccctg ttttgaaaaa";
        char[] charArray = sry.toCharArray();
        Map<Character, Integer> resMap = new HashMap();
        for (char c : charArray) {
            if(resMap.containsKey(c)){
                resMap.put(c,resMap.get(c)+1);
            }else {
                resMap.put(c,1);
            }
        }
        System.out.println(resMap);
    }
}
