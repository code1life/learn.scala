package ch2funcprog

object FuncProgreduce {
  def main(args: Array[String]): Unit = {
    val num = List(1, 2, 3, 4, 5, 6)
    println(num.reduce(sum))
    println(num.reduceLeft(sum))
    println(num.reduceRight(sum))

    println(s"Max value:${num.reduce(max)}")
  }

  def sum(num1: Int, num2: Int): Int = {
    println(s"num1+num2:${num1 + num2}")
    num1 + num2
  }

  def max(num1: Int, num2: Int): Int = {
    if (num1 >= num2) {num1} else {num2}
  }
}
