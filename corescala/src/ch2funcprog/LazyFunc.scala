package ch2funcprog

object LazyFunc {
  def main(args: Array[String]): Unit = {
    lazy val f1 = lazyFunc()
    val f2 = lazyFunc()
    println("Test lazy function.~~~")
    f1
  }

  def lazyFunc(): Unit ={
    println("Lazy function executed")
  }

}
