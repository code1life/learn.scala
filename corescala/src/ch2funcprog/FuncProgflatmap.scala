package ch2funcprog

object FuncProgflatmap {
  def main(args: Array[String]): Unit = {
    val list = List("Adam","Bob","Cherry")
    println(list.map(upperCase))
    println(list.flatMap(upperCase))
  }
  def upperCase(str:String):String={
    str.toUpperCase
  }
}
