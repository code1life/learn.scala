package ch1syntaxbasic

import scala.io.StdIn

object KeyboardInput {
  def main(args: Array[String]): Unit = {
    println("请输入姓名，年龄")
    var name:String = StdIn.readLine()
    var age:Int=StdIn.readInt()
    printf("Name:%s, age:%d",name,age)
  }

}
