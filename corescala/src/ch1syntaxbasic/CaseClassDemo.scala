package ch1syntaxbasic

object CaseClassDemo {
  def main(args: Array[String]): Unit = {
    val cart = Bundle("C语言", 12, Book("C语言程序入门", 20), Food("咖啡", 10.0), Bundle("Java", 10, Book("Java8进阶", 25), Book("Hibernate入门", 36)))
    println(total(cart))
  }

  def total(items: Goods): Double = {
    items match {
      case Book(_, price) => price
      case Food(_, price) => price
      case Bundle(_, discount, others@_*) => others.map(total).sum - discount
    }
  }
}

sealed abstract class Goods

case class Book(name: String, price: Double) extends Goods {}

case class Food(name: String, price: Double) extends Goods {}

case class Bundle(name: String, discount: Double, bandledGoods: Goods*) extends Goods {}