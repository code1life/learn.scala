package ch1syntaxbasic

import scala.collection.mutable.ArrayBuffer

object MatchSyntax {
  def main(args: Array[String]): Unit = {
    var str = "129+-a"
    var num1 = 0
    var num2 = 0
    for (item <- str) {
      item match {
        case '1' => {
          num1 = 1;
          println(num1 + num2)
        }
        case '2' => {
          num2 = 2;
          println(num1 + num2)
        }
        //case mychar => if (item.equals('a')) {println(num1 + num2 + mychar.toInt)}
        case _ => println("+/-")
        //case _ => {println("two default")}
      }
    }

    /*val obj = StdIn.readf1("")//只是为了得到Any返回类型
    obj match {
      case a: String => println("String type" + a.getClass)//obj = a
      case _: Int => println("Int type")// _ = obj,只是不作处理，后面不引用
      case Double => println("Double type")
      case _ =>
    }*/

    println("**********匹配List*********")
    for (list <- Array(List(0), List(1, 2), List(3, 4, 5), List(0, 1), List(0, 0), List(0, 0, 1))) {
      println(list match {
        case 0 :: Nil => 0
        case x :: y :: Nil => println(s"${x}\t${y}")
        case 0 :: tail => println(s"0,${tail}")
        case _ => println("no rexp")
      })
    }
    println("**********匹配Array*********")
    for (arr <- List(Array(0), Array(1, 2), Array(3, 4, 5))) {
      arr match {
        case Array(0) => "0"
        case Array(x, y) => println(s"${y}\t${x}")
        case Array(x, _*) => println(s"${x},...")
      }
    }
    println("--------")
    for (arr <- List(Array(0), Array(1, 2), Array(3, 4, 5))) {
      val res = arr match {
        case Array(0) => 0
        case Array(x, y) => /*ArrayBuffer(y, x)*/Array(y,x).toBuffer
        case Array(x, _*) => Array(0, x).toList
      }
      println(res)
    }

    println("*********匹配Tuple**********")
    for (item <- List((0, 0), (0, 1), (0, 1, 2), (3, 4, 5))) {
      var res = item match {
        case (x, 0) => (3, x)
        case (0, _) => (1, 1)
        case _ => (6, 6)
      }
      println(res)
    }
  }
}
