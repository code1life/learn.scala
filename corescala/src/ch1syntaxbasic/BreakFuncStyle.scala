package ch1syntaxbasic

import scala.util.control.Breaks._

object BreakFuncStyle {
  def main(args: Array[String]): Unit = {
    breakFoo2
    breakFoo1
  }

  def breakFoo1(): Unit = {
    for (i <- 1 to 6) {
      println(s"i is ${i}")
      if (i == 3) {
        break()
      }
    }
    println("Oops, Exception")
  }

  def breakFoo2(): Unit = {
    breakable(//Scala程序员喜欢将小括号换成大括号
      for (i <- 1 to 6) {
        println(s"i is ${i}")
        if (i == 3) {
          break()//也可以在while，do-while中使用
        }
      }
    )
    println("Oops, Exception")
  }
}
