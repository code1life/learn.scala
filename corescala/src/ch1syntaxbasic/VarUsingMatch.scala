package ch1syntaxbasic

object VarUsingMatch {
  def main(args: Array[String]): Unit = {
    val (num1, num2) = (2, 1)
    val (num3, num4) = BigInt(8)/%2
    val arr = Array(1,2,3,4,5)
    val Array(first,second,_*)=arr
    println(s"num1:\t${num1}\nnum2:\t${num2}\nnum3:\t${num3}\nnum4:\t${num4}\nfirst:\t${first}\nsecond:\t${second}")
  }
}
