package ch1syntaxbasic

object NumConvert {
  def main(args: Array[String]): Unit = {
    //强制类型转换只针对最近的操作数有效
    var num1: Int = 10 * 3.5.toInt + 6 * 1.5.toInt
    var num2: Int = (10 * 3.5 + 6 * 1.5).toInt
    println(s"$num1, ${num2}")

    var num3: Byte = 3
    //num3 = num3 + 4
    num3 = (num3 + 4).toByte

    val num4: Byte = 5
    val num5: Char = 1
    var num6 = num4 + num5//类型推导

  }
}
