package ch1syntaxbasic

object OperationRule {
  def main(args: Array[String]): Unit = {

    //华氏->摄氏
    val far=120
    val cen = 5.0/9*(far-100)
    println(cen.formatted("%.2f"))

    println("Hello", "Mr. " + "zhang", "morning")//逗号运算符优先级最低

    println("F: %d, C: %f", far,cen/2);
    printf("F: %d, C: %f", far,cen/2);//格式化输出
  }
}
