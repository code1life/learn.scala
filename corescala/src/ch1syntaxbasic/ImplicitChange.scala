
//隐式转换是编译器在编译的时候作了相应转换，实质上还是强制类型转换
object ImplicitChange {
  def main(args: Array[String]): Unit = {

    //scalac -feature ImplicitChange.scala 查看警告
    import scala.language.implicitConversions //警告解决方法1 //方法2：set compiler option: -language:implicitConversions
    implicit def int2Double(num:Double):Int={//必须写前面，函数名无所谓，只与函数签名（参数和返回值类型）有关
      num.toInt
    }
    val num1:Int=10
    val num2:Int=10.2

    println(s"num1:${num1}\nnum2:${num2}")
  }
}
