package ch1syntaxbasic

object Hello {
  def main(args: Array[String]): Unit = {

    val ++ = "Hello Scala - " //底层转换为$plus$plus
    val +- = 100  //$plus$minus
    println(++ + +-)

    /** Scala关键字
      * abstract case catch class
      * def do else extends false
      * final finally for forSome
      * if implicit import lazy macro match new null object
      * override    package     private protected return sealed super this throw trait       try         true
      * type val var while with yield _ : = => <- <: <% >: # @
      * ---------------------
      * 作者：东陆之滇
      * 来源：CSDN
      * 原文：https://blog.csdn.net/zixiao217/article/details/77132027
      * 版权声明：本文为博主原创文章，转载请附上博文链接！
      */

    //Int可以作标识符，因为其在Scala中不是关键字，是预定义标识符
    val `true` = "关键字作标识符"// String true = "关键字作标识符"
    println(`true`)

    println("Hello Scala!")
  }
}
