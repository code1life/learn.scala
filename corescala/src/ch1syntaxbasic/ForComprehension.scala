package ch1syntaxbasic

//while语句没有返回值，所以需要返回结果就不可避免要使用变量
//而变量声明在while循环的外部，那么就等同于循环的内部对外部变量造成了影响
//所以不推荐使用while，而推荐使用for （for内部变量为val类型）
//Scala语言设计想要达到的效果：循环,函数等尽量不使用外部变量（函数用递归来达到不使用外部变量，递归中变量的加减在函数内（传参时））
object ForComprehension {
  def main(args: Array[String]): Unit = {
    var start = 1
    val end = 10
    //[start, end]
    for (i <- start to end) {
      println("Current num is: " + i)
    }

    val list = List("Hello", 1, 'a', "Scala")
    for (l <- list) {
      println(s"Item[${start}] is " + l);
      start += 1
    }

    //[start, end)
    val i = 1;
    for (item <- i until 11) { //for(item <- i until(11))
      println(s"i add from ${i}, current is " + item)
    }

    //for循环里面的循环变量是val,不能变化，通过使用Range处理变长
    for (i <- Range(1, 5, 2)) {
      println("Current is " + i)
    }

    //for guard
    //功能上类似于其他语言中的break,continue，但不是面向对象，所以去掉这种语法
    for (i <- 1 to 5 if i != 2) {//if是关键字，所以if前没有分号
      println("Current i is " + i)
    }
    //用函数式方法处理,breakable(), break()，例子见BreakFuncStyle

    //引入变量
    for (i <- 1 to 5; j = i * 2) {
      println(s"i: ${i}\tj: ${j}")
    }
    //等价写法(小括号换成大括号)
    for {i <- 1 to 5; j <- 2 to 3} {
      println(s"i: ${i}\tj: ${j}")
    }

    //循环嵌套
    //如果有业务逻辑判断使用传统写法
    for (i <- 1 to 5; j <- 1 to 3; k <- 1 to 2) {
      println(s"i: ${i}\t\t j: ${j}")
    }

    //yield得到的是Vector
    println(for (i <- 1 to 5) yield i * 2)
    //scala喜欢将一个集合交给函数，不断处理后返回新的结果
    //yield接受代码块
    println(for (i <- 1 to 5) yield {
      if (i % 2 == 0) {
        i * 3
      }
      else {
        "不是偶数"
      }
    })


  }
}
