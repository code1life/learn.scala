package ch1syntaxbasic

import scala.io.StdIn

object MultiIf {
  def main(args: Array[String]): Unit = {
    println("小明你的考试成绩是多少：")
    val grade = StdIn.readDouble()
    if (grade == 100){
      println("BMW")
    }else if (grade >=80 && grade <=99){
      println("iPhone7")
    }else if (grade >= 60 && grade < 80){
      println("iPad")
    }else{
      println("Nothing")
    }
  }
}
