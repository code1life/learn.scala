package ch4collection

import scala.collection.mutable.ArrayBuffer

object ArrayBufferBasic {
  def main(args: Array[String]): Unit = {
    val arrayBuffer1 = new ArrayBuffer[Any]()
    val arrayBuffer2 = ArrayBuffer[Any](12, "abc", 'd')

    arrayBuffer1.append(1, "ab", 'c', 3.14, 100f)
    arrayBuffer1(3) = 'c'
    arrayBuffer1.remove(4)

    for (item <- arrayBuffer1) {
      println(s"Cur()\t${item}")
    }
  }
}
