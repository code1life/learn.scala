package ch4collection

object MatchInFor {
  def main(args: Array[String]): Unit = {
    val map = Map(1 -> 10, 2 -> 20, 3 -> 30)
    for ((k, v) <- map) {
      println(s"${k}\t${v}")
    }
    println("-----------------")
    for ((k, 20) <- map) {
      println(s"${k}\t20")
    }
    for ((k, v) <- map if v == 20) {
      println(s"${k}\t20")
    }
  }
}
