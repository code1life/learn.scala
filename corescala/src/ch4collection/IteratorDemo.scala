package ch4collection

object IteratorDemo {
  def main(args: Array[String]): Unit = {
    val list = List(1, 2, 3, 4)
    val it = list.iterator
    while (it.hasNext) {
      println(s"while: Cur is ${it.next()}")
    }
    for (item <- list.iterator) {
      println(s"for: Cur is ${item}")
    }
  }
}
