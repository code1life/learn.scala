package ch4collection

object ViewDemo {
  def main(args: Array[String]): Unit = {

    def palindrome(num:Int):Boolean={
      println("palindrome(num:Int):Boolean")
      num.toString.reverse.equals(num.toString)
    }
    println((9 to 13).filter(palindrome))

    println((9 to 13).view.filter(palindrome))//view lazy exe
    println((9 to 13).view.filter(palindrome).head)//exe;SeqViewF(...)
    println((9 to 13).view.filter(palindrome).tail)//exe;SeqViewFS(...)
    println((9 to 13).view.filter(palindrome).tail.tail)//exe;SeqViewFSS(...)
    for(item<-(9 to 13).view.filter(palindrome)){println(s"item:\t${item}")}
  }
}
