package ch4collection

import scala.collection.mutable

object QueueBasic {
  def main(args: Array[String]): Unit = {
    val queue1 = new mutable.Queue[Any]()
    println(s"queue1:${queue1}")
    queue1 += 1
    println(s"queue1:${queue1}")
    var queue2 = queue1 :+ "efg"
    println(s"queue2:${queue2}")


    val list1 = List[Any](1, 2, "ab")
    queue1 += list1
    println(s"queue1:${queue1}")
    queue1 ++= list1 //List必须声明为Any类型,注意list加的位置
    println(s"queue1:${queue1}")


    val queue3 = new mutable.Queue[Int]()
    queue3.enqueue(1)
    queue3.enqueue(1,2,3,4,5,6)
    println(s"queue3.dequeue()->${queue3.dequeue()}")
    println(s"queue3:${queue3}")

    println(s"queue3.head:${queue3.head}")//都是函数
    println(s"queue3.front:${queue3.front}")
    println(s"queue3.tail:${queue3.tail}")//除了第一个元素
    println(s"queue3.tail.tail:${queue3.tail.tail}")
    println(s"queue3.last:${queue3.last}")
  }
}
