package ch4collection

object ListBasic {
  def main(args: Array[String]): Unit = {
    val listl = List(1, true, "abc")
    println(listl(1))
    for (item <- listl){
      print(item + "\t")
    }

    val list2 = listl :+ 'd' //List不可变，因此重新生成了一个List
    println(s"\nlistl:${listl}")//注意元素的顺序
    println(s"list2:${list2}")
    val list3 = 2 +: list2 //冒号的位置体现List的有序性
    println(s"list3:${list3}")

    val list4 = 2 :: 3 :: 4 :: Nil  //2 :: 3 :: 4 +: Nil；集合对象要在最右边
    println(s"list4:${list4}")
    val list5 = "bd" +: list4       //"bd" :: list4
    println(s"list5:${list5}")

    val list6 = "ac" +: "bd" :: list4 :: Nil//::运算符优先级从右到左
    println(s"list6:${list6}")
    val list7 = "ac" +: "bd" :: list4 ::: Nil//:::将集合中每一个元素加入到集合中去
    println(s"list7:${list7}")
    val list8 = "ac" ++: "bd" :: list4 ::: Nil
    println(s"list8:${list8}")


  }
}
