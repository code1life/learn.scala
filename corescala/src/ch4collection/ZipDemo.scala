package ch4collection

object ZipDemo {
  def main(args: Array[String]): Unit = {
    val list1 = List(1, 2, 3)
    val list2 = List(4, 5, 6, 7)
    println(list1.zip(list2))//zip不限于List

    for (item <- list1.zip(list2)) {
      println(item._1 + "\t" + item._2)
    }
  }
}
