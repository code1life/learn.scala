package ch4collection

object TupleBasic {
  def main(args: Array[String]): Unit = {
    val tuple1 = (1,2,"abc",'d')
    println(tuple1.getClass)
    println(tuple1)

    println(tuple1._1)//下标从1开始
    println(tuple1.productElement(0))//模式匹配

    for(it <- tuple1.productIterator){
      print(it+"\t")
    }
  }
}
