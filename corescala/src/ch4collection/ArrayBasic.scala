package ch4collection

object ArrayBasic {
  def main(args: Array[String]): Unit = {
    val ints = new Array[Int](3) //val ints = new Array[Any](3)
    //ints.toBuffer
    for (item <- ints) {println(s"Cur():\t${item}")}
    ints(2) = 10
    for (item <- ints) {println(s"Cur:\t${item}")}

    val array = Array(1, "ABC", 'a')
    for (item <- array) {println(s"Cur():\t${item}")}
    for (index <- 0 to array.length - 1) {println(s"Cur(${index}):\t${array(index)}")}
    for (index <- 0 until array.length) {println(s"Cur(${index}):\t${array(index)}")}

    val multiDimArray = Array.ofDim[Int](3, 4)
    multiDimArray(1)(2) = 3
    for (d1 <- multiDimArray) {
      for (d2 <- d1) {
        print(d2 + "\t")
      }
      println()
    }
  }
}
