package ch4collection

import scala.collection.mutable.ListBuffer

object ListBufferBasic {
  def main(args: Array[String]): Unit = {
    val listBuffer = ListBuffer[Int](1, 2, 3, 45)
    println(s"listBuffer(1)\t:${listBuffer(1)}")
    for (item <- listBuffer) {
      print(s"${item}\t")
    }
    println()

    val listBuffer1 = new ListBuffer[Int]
    listBuffer1 += 6 //增加单个元素
    listBuffer1 :+ 7
    listBuffer1.append(8, 9) //增加多个元素

    listBuffer1 ++= listBuffer //listBuffer1 = listBuffer1 ++ listBuffer
    for (item <- listBuffer1) {
      print(item + "\t")
    }
    println()

    listBuffer1.remove(2, 2)
    for (item <- listBuffer1) {
      print(item + "\t")
    }
  }
}
