package ch4collection

object CollectionParallel {
  def main(args: Array[String]): Unit = {
    (1 to 5).map(println(_))
    (1 to 5).par.map(println)

    println((1 to 100).map{case _ => Thread.currentThread()}.distinct)
    println((1 to 100).par.map{case _ => Thread.currentThread()}.distinct)
  }
}
