package ch4collection

import scala.collection.mutable

object MapBasic {
  def main(args: Array[String]): Unit = {
    val map1 = Map("BJ" -> "北京", "SH" -> 28, "GZ" -> "广州") //有序
    println(s"map1:\t${map1}")

    val map2 = mutable.Map("BJ" -> "北京", "SH" -> "上海", "GZ" -> "广州")
    map2.put("SZ", "深圳")
    println(s"map2:\t${map2}")

    val map3 = mutable.Map(("BJ", "北京"), ("SH", "上海")) //二元组形式写Map
    println(s"map3:\t${map3}")

    println(s"map1('BJ'):\t${map1("BJ")}")
    if (map1.contains("BJi")) {
      println(map1("BJ"))
    } else "没有此Key" //contains先检查，防止抛出异常

    println(map1.get("BJ"))
    println(map1.get("BJ").get)
    println(map1.getOrElse("BJI", "默认值"))

    map2("BJ") = "北平"
    println(s"map2('BJ'):\t${map2}")

    map3 += ("CQ" -> "重庆")
    println(s"map3:\t${map3}")
    map3 += ("CQ" -> "重庆", "WH" -> "武汉")
    map3 -= ("CQ" , "重庆")
    println(s"map3:\t${map3}")

    for (key <- map3.keys) {print(key+"\t")};println()
    for ((k, v) <- map3) {println(k + "\t" + v)}
    for (value <- map3.values){print(value+"\t")};println()
    for (item <- map3){println(item);println(item._1+"\t"+item._2)}

  }
}
