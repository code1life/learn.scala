package ch4collection

import scala.collection.mutable

object StreamDemo {
  def main(args: Array[String]): Unit = {
    def numGenerator(num: Int): Stream[Int] = {
      num #:: numGenerator(num + 3)
    }

    println(s"numGenerator(1):\t${numGenerator(1)}")
    println(s"numGenerator(1).head:\t${numGenerator(1).head}")
    println(s"numGenerator(1).tail:\t${numGenerator(1).tail}")

    def multiply(num: Int): Int = {
      num * num
    }
    println(s"${numGenerator(2).map(multiply)}")


  }
}
