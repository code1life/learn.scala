package ch3oop {

  package object packageobject0 {
    var pObjVar = "package object variable can be used by corresponding package"

    def foo: Unit = {
      println("package object function executed")
    }
  }

  package packageobject0 {

    class PacObj0(conPObj0: String) {
      var pacObj0Var: String = conPObj0


      def printField: Unit = {
        println(s"pObj0Var=${pacObj0Var}")
      }
    }

    object pObj1 {
      def main(args: Array[String]): Unit = {
        println(pObjVar)
        foo

        val pacObj0 = new PacObj0(pObjVar)
        pacObj0.printField
      }
    }

  }

  package packageobject1 {

    class PacObj1 {

    }

    object pacObj2 {
      def main(args: Array[String]): Unit = {
        println("Another package main func")
      }
    }

  }

}
