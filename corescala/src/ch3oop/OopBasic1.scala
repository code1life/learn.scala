package ch3oop

object OopBasic1 {
  def main(args: Array[String]): Unit = {
    var subBase1 = new SubBase
    var subBase2 = new SubBase("using alternative constructor")
  }
}

class Base {
  println("exe:\tBase constructor")
}

class SubBase extends Base {

  println("exe:\tmain constructor")

  def this(name:String) {
    this//调用了主构造器，主构造器中默认已经调用了父类构造器,所以不能super()
    println("exe:\tSubBase alternative constructor"+s" - ${name}")
  }

  def this(name:String, age:Int){
    this//此处也必须调用主构造器 - 直接调用
    //do something else
  }

  def this(name:String,age:Int,sex:Char){
    this("张三")//因为this(name:String)已经调用了主构造函数，所以此处不需要调用主构造函数 - 间接调用
  }
}