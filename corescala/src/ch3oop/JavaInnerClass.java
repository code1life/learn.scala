package ch3oop;

public class JavaInnerClass {
    public static void main(String[] args) {
        OuterClass outerClass = new OuterClass();
        outerClass.printInfo();
        OuterClass.InnerClassA innerClassA = outerClass.new InnerClassA();//Java把内部类当作属性看待
        innerClassA.printInfo();
        OuterClass.InnerClassB innerClassB = new OuterClass.InnerClassB();
        innerClassB.printInfo();

        OuterClass outerClass1 = new OuterClass();
        OuterClass.InnerClassA innerClassA1 = outerClass1.new InnerClassA();
        innerClassA.foo(innerClassA);
        innerClassA.foo(innerClassA1);//Scala不能这样用
    }
}

class OuterClass{
    void printInfo(){
        System.out.println("OuterClass::printInfo()");
    }
    class InnerClassA{
        void printInfo(){
            System.out.println("InnerClassA::printInfo()");
        }
        void foo(InnerClassA innerClassA){
            System.out.println("InnerClassA::foo(InnerClassA innerClassA)");
        }
    }
    static class InnerClassB{
        void printInfo(){
            System.out.println("InnerClassB::printInfo()");
        }
    }
}