package ch3oop

object ImplicitVar {
  def main(args: Array[String]): Unit = {
    implicit var str1 = "Adam"
    //implicit var num1 = 10

    def foo(implicit str2: String): Unit = {
      println(s"Hello ${str2}")

      def foo(): Unit = {
        println("hello(){hello(){}}")
      }

      foo()
    }

    foo
    foo("Bob")

    def foo1(implicit str3: String = "Cylon"): Unit = {
      println(s"Hello ${str3}")
    }

    foo1

    def foo2(implicit num2: Int = 100): Unit = {
      println(s"Res: ${num2}")
    }
    foo2

    def foo3(implicit num3:Double): Unit ={
      println(s"Res: ${num3}")
    }
    //foo3
  }
}