package ch3oop

object Implicit4Class {

  def main(args: Array[String]): Unit = {

    val ariaDB = new AriaDB
    ariaDB.insert()
    ariaDB.delete()//增加了功能

    implicit def ariaDBAddDelete(ariaDB: AriaDB): DataBase ={//可以放在main外，在开发中会将所有的隐式转换放到一个文件中，然后导入。
      new DataBase
    }
  }
}

class AriaDB {
  def insert(): Unit = {
    println("AriaDB:insert(): Unit")
  }
}

class DataBase {
  def delete(): Unit = {
    println("DataBase:delete(): Unit")
  }
}