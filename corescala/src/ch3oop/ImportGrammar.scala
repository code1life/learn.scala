package ch3oop


object ImportGrammar {
  def main(args: Array[String]): Unit = {

  }
}

class ImportGrammarA {
  import scala.beans.BeanProperty
  @BeanProperty var name: String = _
}

class ImportGrammarB {
  var name: String = _
  //@BeanProperty var age:Int=_   //import引入只作用于ImportGrammarA
}
