package ch3oop

object TraitMixin {
  def main(args: Array[String]): Unit = {
    val mysqlDB = new MySQLDB with DB
    mysqlDB.insert("ABC-MySQL")

    val oracleDB = new OracleDB with DB
    oracleDB.insert("ABC-Oracle")

    val sqlserver = new SQLserverDB with DB {
      override def echo(): Unit = {
        println("echo func")
      }
    }
    sqlserver.insert("ABC-SQLServer")
    sqlserver.echo()
  }
}

trait DB{
  def insert(item:String): Unit ={
    println(s"insert into db with ${item}")
  }
}

class MySQLDB{}
abstract class OracleDB{}
abstract class SQLserverDB{
  def echo()
}