package ch3oop

object OopBasic {
  def main(args: Array[String]): Unit = {
    val fish = new Fish("草鱼", 9.0f)
    println(fish)
    println(fish.soldNumber(10, 20))
  }

}

class Fish(conName: String, conPrice: Float) {
  var name: String = conName
  private var price: Float = conPrice

  def soldNumber(n1: Int, n2: Int): Int = {
    return n1 + n2
  }

  override def toString: String = {
    "品种：" + this.name + "\t价格：" + this.price
  }

  println("change field value")
  price += 5
  println("done ~ things underground (all(besides method) put into constructor) -> grammar sugar")

  def this(name: String) {
    //this()
    this("鲤鱼",12.0f)//必须显式先调用主构造函数,如果调用无参主构造器，此处可以直接写this
    this.name = name
  }
}

