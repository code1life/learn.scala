package ch3oop

object ApplyUnapply {
  def main(args: Array[String]): Unit = {
    val s = /*Square(6)*/ 36.0 //36则匹配不到，类型为Int
    s match {
      case Square(n) => {//Square(n)会调用unapply()，规定如果返回Some类型，则匹配，返回None则不匹配
        println(n)
      }
      case _ =>
    }

    val n = "Adam,Bob,Cherry"
    n match {
      case Name(first,second,third) => {//多个参数调用unapplySeq()
        println(s"${first}\t${second}\t${third}")
      }
      case _ =>
    }
  }
}

object Square {
  def apply(num: Double): Double = num * num

  //对象提取器
  def unapply(arg: Double): Option[Double] = {
    println(s"unapply(arg: Double): Option[Double]\t${arg}")
    Some(math.sqrt(arg))
  }
}

object Name {
  def unapplySeq(arg: String): Option[Seq[String]] = {
    if (arg.contains(",")) {
      Some(arg.split(","))
    } else None
  }
}