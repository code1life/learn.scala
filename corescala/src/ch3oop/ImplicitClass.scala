package ch3oop

object ImplicitClass {
  def main(args: Array[String]): Unit = {

    implicit class ClassB(val clsA:ClassA){
      def delete(): Unit ={
        println("ClassB:delete()")
      }
    }

    val classA = new ClassA
    classA.save()
    classA.delete()//

  }
}

class ClassA{
  def save(): Unit ={
    println("ClassA:save()")
  }
}

/*
implicit class ClassB(val clsA:ClassA){//implicit modifier can't be used for top-level objects
  def delete(): Unit ={
    println("ClassB:delter()")
  }
}*/
