package ch3oop

object ClassExtends {
  def main(args: Array[String]): Unit = {
    val stu = new ClassExtendsB
    stu.name = "Adam" //调用了ClassExtendsA的public String name()方法
    println(stu.show)
    stu.study
  }
}

class ClassExtendsA (conAge:Int) {
  var name: String = _
  var age: Int = conAge

  def show: String = {
    "name:" + name + "\tage:" + age
  }
}

class ClassExtendsB extends ClassExtendsA(10) {
  def study: Unit = {
    println("I'am a student, study is my main task.")
  }
}