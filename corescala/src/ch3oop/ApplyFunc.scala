package ch3oop

object ApplyFunc {
  def main(args: Array[String]): Unit = {
    val list = List(1,3,4,'a',"abc")
    println(list)

    val obj1 = new ObjWithApplyFunc("Test1")
    val obj2 = new ObjWithApplyFunc("Test2")
    val obj3 = ObjWithApplyFunc()//实现类似于上面的List，因为调用了伴生对象中的apply()方法
    println(s"obj1:${obj1.name}\nobj2:${obj2.name}\nobj3:${obj3.name}")
  }
}

class ObjWithApplyFunc(conName:String){
  var name:String=conName
}
object ObjWithApplyFunc{
  def apply(conName: String): ObjWithApplyFunc = new ObjWithApplyFunc(conName)

  def apply(): ObjWithApplyFunc = new ObjWithApplyFunc("默认")
}
