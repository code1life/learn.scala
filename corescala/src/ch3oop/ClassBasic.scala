package ch3oop

import scala.beans.BeanProperty

object ClassBasic {
  def main(args: Array[String]): Unit = {
    val a = new ClassBasicA
  }
}

class ClassBasicA {
  private var name: String = _
  //private String name;
  //private String name();
  //private void  name_$eq(String x$1)
  var age: Int = _
  //private int age;
  //public int age();
  //public void age_$eq(int x$1)
  @BeanProperty var salary: Double = _
  //private double salary;
  //public double salary();
  //public void salary_$eq();
  //public double getSalary();
  //public void setSalary(double x$1)
}