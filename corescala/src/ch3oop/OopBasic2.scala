package ch3oop

object OopBasic2 {
  def main(args: Array[String]): Unit = {
    val v = new Basic2("Although can't exe default constructor, it still can exe override constructor")
  }

}

class Basic2 private() {  //class Basic2 private { 默认构造的话这样写也可以

  var name: String = _
  var age: Int = _
  println("Demo about private constructor")

  //private
  def this(name: String) {
    this
    println(name)
  }

  /*private this (name: String, age: Int) {
    this.name = name
    this.age = age
  }*/

}