package ch3oop

object AbstractClass {
  def main(args: Array[String]): Unit = {
    println("Grammar is right")
  }

}

abstract class AbstractClassA {
  var name: String//抽象属性
  var age: Int = 10
  def foo1()//抽象方法不加abstract，没有方法体
  def foo2(): Unit ={//抽象类可以有实现的方法
    println("exe foo2()")
  }
}
class AbstractClassB extends AbstractClassA{
  override var name: String = ""
  //override var age:Int = 10 //variable age can't override a mutable variable
  override def foo1(): Unit = { //override可以去掉
    println("Implement abstract function")
  }
}
