package ch3oop

import scala.beans.BeanProperty

object OopBasic3 {
  def main(args: Array[String]): Unit = {
    val v1 = new Basic3a("Adam")
    println(v1.name)

    val v2 = new Basic3b("Bob")
    //v2.conName=""
    println(v2.name)
    println(v2.conName) //底层会把conName转换成一个静态私有变量（private final String conName;）

    val v3 = new Basic3c("Cindy")//底层会把conName转换成一个私有变量（private String conName;）
    v3.conName="Gorge"
    println(v3.name)
    println(v3.conName)
  }
}

class Basic3a(conName: String) {
  @BeanProperty var name: String = conName
}

class Basic3b(val conName: String) {
  var name: String = conName
}

class Basic3c(var conName: String) {
  var name: String = conName
}