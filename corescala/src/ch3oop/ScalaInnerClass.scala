package ch3oop

object ScalaInnerClass {
  def main(args: Array[String]): Unit = {
    val outerClass = new OuterClassScala
    outerClass.printInfo()
    val innerClassA = new outerClass.InnerClassScalaA
    innerClassA.printInfo()
    val innerClassB = new OuterClassScala.InnerClassScalaB
    innerClassB.printInfo()

    val outerClass1 = new OuterClassScala
    val innerClassA1 = new outerClass1.InnerClassScalaA
    innerClassA.foo(innerClassA)
    //innerClassA.foo(innerClassA1) //Java可以，Scala中因为外部类的对象不一样
    innerClassA.foo(innerClassA1) //通不过的时候，先不使用投影运行一下
  }
}

class OuterClassScala {
  var name: String = "Adam"
  private val age: Int = 20

  def printInfo(): Unit = {
    println("OuterClassScala::printInfo()")
  }

  class InnerClassScalaA {
    def printInfo(): Unit = {
      println("InnerClassScalaA::printInfo()")
      println(s"name:${name}\tage:${age}")
      println("name:" + OuterClassScala.this.name + "\tage:" + OuterClassScala.this.age)
    }

    def foo(innerClassScalaA: OuterClassScala#InnerClassScalaA): Unit = { //def foo(innerClassScalaA:InnerClassScalaA): Unit = { //投影
      println("InnerClassScalaA::foo(): Unit")
      println(innerClassScalaA)
    }
  }

  //myOutClass => //似乎不支持外部类的别名了
  class InnerClassScalaC {
    def printInfo(): Unit = {
      println("InnerClassScalaC::printInfo()")
      println(s"name:${name}\tage:${age}")
      println("name:" + OuterClassScala.this.name + "\tage:" + OuterClassScala.this.age)
    }
  }

}

object OuterClassScala {

  class InnerClassScalaB {
    def printInfo(): Unit = {
      println("InnerClassScalaB::printInfo()")
    }
  }

}