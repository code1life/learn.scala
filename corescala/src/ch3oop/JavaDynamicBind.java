package ch3oop;

public class JavaDynamicBind {
    public static void main(String[] args) {
        JavaDynamicBindBase obj = new JavaDynamicBindSub();
        System.out.println(obj.sum());
        System.out.println(obj.anotherSum());
    }
}

class JavaDynamicBindBase {
    private int num = 10;

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int sum() {
        return num + 10;
    }

    public int anotherSum() {
        return getNum() + 10;
    }
}

class JavaDynamicBindSub extends JavaDynamicBindBase {
    private int num = 20;

    @Override
    public int getNum() {
        return num;
    }

    @Override
    public void setNum(int num) {
        this.num = num;
    }

    public int sum() {
        return num + 20;
    }

    public int anotherSum() {
        return getNum() + 20;
    }
}