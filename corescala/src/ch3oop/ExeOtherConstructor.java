package ch3oop;

public class ExeOtherConstructor {
    public static void main(String[] args) {
        new JavaTest();
    }
}

class JavaTest {
    JavaTest() {
        this("execute other constructor of the same class");//必须第一行，且不能调用super()
        System.out.println("exe:\tdefault constructor");
    }

    JavaTest(String name) {
        //super()此处默认已经有了，所以在默认构造函数中不能写super()
        System.out.println("exe:\tconstructor with parameter" + " - " + name);
    }
}